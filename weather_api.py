# Imports
import requests # Used to access web data
import pandas as pd
from IPython.display import display 
import yaml # Used as config
import matplotlib.pyplot as plt # Plots results
import country_location as cl # Imports country data
from prompt_toolkit import prompt
from prompt_toolkit.completion import WordCompleter # Used for autocomplete

# Pulls country data from country_location.py
country_df, country_list = cl.get_country_locations()

# Reads and opens config file to use parameters
with open('config.yaml') as f:
    config = yaml.load(f, Loader=yaml.FullLoader)

# Function to save any changes made to config file within app
def save_config():

    global config
    with open('config.yaml', 'w') as f:
        config = yaml.dump(config, stream=f,
                        default_flow_style=False, sort_keys=False)

def get_weather():
    '''
    Obtains weather data through the open-meteo weather api using location.
    Locations are obtained using the country_location.py module.
    Users can select locations by typing or selecting from the autocomplete dropdown.
    Function checks the config parameters to see what weather data to pull and display
    '''
    # Prints entire list of countries for users to see
    print(country_list)
    print('\n')
    
    # Contains the function in a loop 
    while True:

        # Uses prompt_toolkit and WordCompleter to create a dropdown for locations
        country = prompt('Type a country or press (0) to return: ', completer = WordCompleter(country_list))

        # Breaks out of loop
        if country == '0':
            break
        # Checks to make sure input is valid
        elif country.title() not in country_list:
            print('Country not recognised. Please type a country from the list')
            continue
        # Uses select country name to locate the latitude and longitude values and stores these as variables
        else:
            selected_country = country_df.loc[country_df['name'] == country.title()]

            latitude = float(selected_country['latitude'].values)
            longitude = float(selected_country['longitude'].values)

            #print(latitude, longitude)

            # Calculates number of True parameters because different combination of parameters may or may not need commas in their strings for the url to work
            x = 0
            for parameter in config.values():
                if parameter is True:
                    x += 1

            # Case for Temperature parameter
            if config['Temperature'] is True and x == 1:
                temperature = 'temperature_2m_max,temperature_2m_min'
            elif config['Temperature'] is True and x > 1:
                temperature = 'temperature_2m_max,temperature_2m_min,'
            else:
                temperature = ''

            # Case for Sunrise_set parameter
            if config['Sunrise_set'] is True and config['Precipitation'] is True:
                sunrise_set = 'sunrise,sunset,'
            elif (config['Sunrise_set'] is True and x == 1) or (config['Sunrise_set'] is True and config['Temperature'] is True):
                sunrise_set = 'sunrise,sunset'
            else:
                sunrise_set = ''

            # Case for Precipitation parameter
            if config['Precipitation'] is True:
                precipitation = 'precipitation_sum,precipitation_hours'
            else:
                precipitation = ''

            # Creates url using all input parameters
            url = f"https://api.open-meteo.com/v1/forecast?latitude={latitude}&longitude={longitude}&daily={temperature}{sunrise_set}{precipitation}&timezone=auto"
            
            #print(url) # good for debugging the input parameters
            r = requests.get(url)

            # Converts data to json format
            data = r.json()

            # Tries to extract daily data
            try:
                country_weather_df = pd.DataFrame.from_dict(data['daily'])
            except:
                print('Sorry, an error occured')
                continue
            
            # Displays weather data as a dataframe
            display(country_weather_df)

            # Creates plot for data
            fig, ax = plt.subplots() 

            if config['Temperature'] is True:
                country_weather_df.plot(x='time', y = ['temperature_2m_max', 'temperature_2m_min'], ax = ax, title = f"Forecasted Min/Max Temperature in {country.title()}", colormap = 'RdBu')

            if config['Precipitation'] is True:
                country_weather_df.plot(x='time', y = ['precipitation_sum',  'precipitation_hours'], secondary_y = True, ax=ax, title = f"Forecasted Min/Max Temperature in {country.title()}", colormap = 'PiYG')

            plt.show()

def config_menu():
    '''
    Can turn parameters on and off through this config menu. Will alter the config.yaml file and save changes. 
    '''
    global config

    while True:
        # Reads and opens config file for modifying
        with open('config.yaml') as f:
            config = yaml.load(f, Loader=yaml.FullLoader)

        # Inits empty parameters list
        params_list = []
        # Goes through all parameters in config and prints for user to see what is available
        print('\nParameters:')
        for parameters, value in config.items():
            # Appends parameters to the empty list for use in the prompt line
            params_list.append(parameters)
            print(f"{parameters}: {value}")

        # Uses prompt_toolkit and WordCompleter to create a dropdown for parameters list
        param_select = prompt('Select a parameter or (0) to return: ', completer = WordCompleter(params_list))

        # Different cases for user input
        if param_select == '0':
            break
        elif param_select not in params_list:
            print('Parameter not valid')
            continue
        # Provides options to enable, disable or exit current selection
        else:
            param_enab = input('Press (1) to enable. Press (2) to disable. Press (0) to exit ')
            # Sets parameter to "true"
            if param_enab == '1':
                config[param_select] = True
                save_config()
                continue
            # Sets parameter to "false"
            elif param_enab == '2':
                config[param_select] = False
                save_config()
                continue
            # Breaks out of loop
            elif param_enab == '0':
                break
            # Ensures robust code
            else:
                print('Invalid input')
                continue

def menu():
    '''
    Main menu function. Every option carries out a unique option
    '''
    while True:
        option = input('Press (1) for weather app. Press (2) for configuration. Press (0) to exit: ')

        if option == '0':
            break
        elif option == '1':
            get_weather()
            continue
        elif option == '2':
            config_menu()
            continue
        else:
            print('Invalid input')
            continue

        break

menu()