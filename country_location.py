import requests
import pandas as pd

def get_country_locations():
    url = 'https://developers.google.com/public-data/docs/canonical/countries_csv'

    r = requests.get(url)

    country_df = pd.read_html(url)

    country_list =[]
    for country in country_df[0]['name']:
        country_list.append(country)
    country_list.sort()

    return country_df[0], country_list

get_country_locations()